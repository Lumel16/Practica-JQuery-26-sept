$(document).ready(function(){
	/*Ocultar todos los div de cada ejercicio*/
	$('.Ejercicios').hide();

	/*Mostrar ejercicio 1*/
	$("#ex1").on('click', function(event) {
		$(".Ejercicio1").toggle();
	});

	/*Ejecuta el ejercicio 1*/
	$('#ej1').on('click', function(event) {
		$('div').find('.Module').html('Este div contiene la clase module');
	});

	/*Mostrar ejercicio 3*/
	$("#ex3").on('click', function(event) {
		$(".Ejercicio3").toggle();
	});

	/*Ejecuta el ejercicio 3*/
	$('#ej3').on('click', function(event) {
		$('label[for="texto"]').html('Texto Cambiado');
	});

	/*Mostrar ejercicio 4*/
	$("#ex4").on('click', function(event) {
		$(".Ejercicio4").toggle();
	});

	/*Ejecuta el ejercicio 4*/
	$('#ej4').on('click', function(event) {
		var num = $('body')
			.children(':hidden')
			.length;
		$('#ocultos').html("Elementos ocultos " + num);
	});

	/*Mostrar ejercicio 5*/
	$("#ex5").on('click', function(event) {
		$(".Ejercicio5").toggle();
	});

	/*Ejecuta el ejercicio 5*/
	$('#ej5').on('click', function(event) {
		var num = $('.Ejercicio5 img[alt]').length;
		$("#contalt").html("Imagenes con atributo alt " + num)
	});

	/*Mostrar ejercicio 6*/
	$("#ex6").on('click', function(event) {
		$(".Ejercicio6").toggle();
	});

	/*Ejecuta el ejercicio 6*/
	$('#ej6').on('click', function(event) {
		$('#filas tr:odd').css('background', '#FF974B');
	});

	/*Mostrar ejercicio 2-1*/
	$("#ex2-1").on('click', function(event) {
		$(".Ejercicio2-1").toggle();
	});

	/*Ejecuta el ejercicio 2-1*/
	$('#ej2-1').on('click', function(event) {
		$('.Ejercicio2-1 img[alt]').each(function(index, el) {
			console.log($(el).attr('alt'));
		});
	});

	/*Mostrar ejercicio 2-2*/
	$("#ex2-2").on('click', function(event) {
		$('.Ejercicio2-2').toggle();
	});

	/*Ejecuta el ejercicio 2-2*/
	$('#ej2-2').on('click', function(event) {
		$('#addClass input').addClass('InputClass');
	});

	/*Mostrar ejercicio 2-3*/
	$("#ex2-3").on('click', function(event) {
		$('.Ejercicio2-3').toggle();
	});

	/*Ejecuta el ejercicio 2-3*/
	$('#ej2-3').on('click', function(event) {
		var lista = $('.Ejercicio2-3 li.current');
		lista.removeClass('current');
		lista.next('li').addClass('current');
	});

	/*Mostrar ejercicio 2-4*/
	$("#ex2-4").on('click', function(event) {
		$('.Ejercicio2-4').toggle();
	});

	/*Ejecuta el ejercicio 2-4*/
	$('#ej2-4').on('click', function(event) {
		$select = $('#specials select');
		$select.addClass('seleccion');
		$select.siblings().addClass('seleccion');
	});

	/*Mostrar ejercicio 2-5*/
	$("#ex2-5").on('click', function(event) {
		$('.Ejercicio2-5').toggle();
	});

	/*Ejecuta el ejercicio 2-5*/
	$('#ej2-5').on('click', function(event) {
		$padre = $('#slideshow li');
		$hijo = $padre.first();
		$hijo.addClass('current');
		$hijo.siblings().addClass('disable');
	});

	/*Mostrar ejercicio 3-1*/
	$("#ex3-1").on('click', function(event) {
		$('.Ejercicio3-1').toggle();
	});

	/*Ejecuta el ejercicio 3-1*/
	$('#ej3-1').on('click', function(event) {
		var add = "";
		for(i=0;i<=4;i++){
			add += '<li> item ' + (i + 3) + '</li>';
		}
		$('#myList').append(add);
	});

	/*Mostrar ejercicio 3-2*/
	$("#ex3-2").on('click', function(event) {
		$('.Ejercicio3-2').toggle();
	});

	/*Ejecuta el ejercicio 3-2*/
	$('#ej3-2').on('click', function(event) {
		$('#removelist li:odd').remove();
	});

	/*Mostrar ejercicio 3-3*/
	$("#ex3-3").on('click', function(event) {
		$('.Ejercicio3-3').toggle();
	});

	/*Ejecuta el ejercicio 3-3*/
	$('#ej3-3').on('click', function(event) {
		$h2 = 'Encabezado generado dinamicamente';
		$parrafo = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Rerum modi quas temporibus nulla, rem expedita debitis iure recusandae similique perspiciatis sed facere, repellendus repudiandae tempore'
		+ 'laborum vero ab, pariatur ducimus repellat omnis. Amet corrupti suscipit, cum porro assumenda laboriosam. Id esse voluptatibus maxime aliquid hic accusamus quae consequuntur praesentium dolore sequi ';
		$('.Ejercicio3-3 .module:last').append('<h2> ' + $h2 + '</h2>' + '<p>' + $parrafo + '</p>');
	});

	/*Mostrar ejercicio 3-4*/
	$("#ex3-4").on('click', function(event) {
		$('.Ejercicio3-4').toggle();
	});

	/*Ejecuta el ejercicio 3-4*/
	$('#ej3-4').on('click', function(event) {
		$('#semana').append('<option>Wednesday</option>');
	});

	/*Mostrar ejercicio 3-5*/
	$("#ex3-5").on('click', function(event) {
		$('.Ejercicio3-5').toggle();
	});

	/*Ejecuta el ejercicio 3-5*/
	$('#ej3-5').on('click', function(event) {
		$('#ultimo').append('<div class="module"></div>');
		$('.Ejercicio2-1 img:first').clone().appendTo('#ultimo .module:last');
	});

	});